## What is this?  
This is a simple IRC bot for GitLab. It takes events from Webhooks and joins a channel to tell everyone you know about them.

## How do I do?
Clone this. Put it in a folder somewhere.
Run `bundle install`.
Configure the IRC info in the gemfile.
Run `ruby gitlab-irc.rb`.

If you want to keep it running in the background, you might want to use something like `tmux` or `screen`.


## Commands
`[i#1]` - Will look up and link issue \#1 for the configured repo.

`[m#1]` - Will look up and link merge request \#1 in the configure repo.

`[name] help` Will link you to this readme. Replace [name] with the configured name of your bot.