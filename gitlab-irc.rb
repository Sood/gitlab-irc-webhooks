require 'rubygems'
require 'sinatra'
require 'json'
require 'logger'
require 'yaml'
require 'socket'
require 'net/http'
Thread.abort_on_exception = true

set :bind, '0.0.0.0'
if ARGV[0]
  Dir.chdir ARGV[0]
end

$logger = Logger.new('log.txt')
$logger.debug "New run!"

class Bot
  def initialize
    $logger.debug("Opening config file: #{Dir.pwd}/config.yml")
    @config = YAML.load_file("config.yml")
    @gitlab = @config['gitlab']
    @config = @config['irc']
    @socket = open_irc
    @welcomed = false
    @apiurl = 'https://gitlab.com/api/v3/'

    # Find project url.
    req = api_request "#{@apiurl}projects/#{@gitlab['project-id']}", {}
    req = JSON.parse(req)

    @projecturl = req['web_url']
    puts "Project URL configured as #{@projecturl} ."
  end

  def open_irc()
    socket = TCPSocket.open(@config['host'], @config['port'])
    socket.puts("NICK #{@config['nick']}")
    socket.puts("USER #{@config['nick']} 8 * : #{@config['realname']}")

    Thread.new do
      until @socket.eof? do
        msg = @socket.gets
        puts "<< " + msg

        if msg.match(/^PING :(.*)$/)
          puts "PONG #{$~[1]}"
          @socket.puts "PONG #{$~[1]}"
          next
        elsif @welcomed == false && msg.match(":End of /MOTD command.")
          puts("Recieved end of MOTD, joining.")
          @welcomed = true
          puts("<< JOIN ##{@config['channel']}")
          socket.puts("JOIN ##{@config['channel']}")
        end

        if msg.match(/PRIVMSG ##{@config['channel']} :(.*)$/)
          content = $~[1]

          #put matchers here
          # Merge Request trigger
          if msg.match(/\[m#(.*)\]/)
            puts "    Merge request: \n    #{msg}"
            m = msg.scan(/\[m#(\d+)\]/)[0]
            puts m
            request_mr m
          # Issue trigger
          elsif msg.match(/\[i#(.*)\]/)
            puts "    Issue: \n    #{msg}"
            i = msg.scan(/\[i#(\d+)\]/)[0]
            puts i
            request_issue i
          #asking for help
          elsif msg.match("#{@config['nick']} help") || msg.match("#{@config['nick']}, help")
            irc_msg 'README: https://gitlab.com/Sood/gitlab-irc-webhooks/blob/master/README.md'
          elsif msg.match(/\[\b([a-f0-9]{40})\b\]/)
            s = msg.scan(/\[\b([a-f0-9]{40})\b\]/)
            request_sha s
          end
        end
      end
    Thread.stop
    end
    return socket
  end

  def close_irc()
    @socket.puts("PART #{@config['channel']}")
    @socket.close
  end

  def irc_msg(message)
    @socket.puts "PRIVMSG ##{@config['channel']} :" + message
  end

  def commits(json)
    if @config['enable_push_msg'] != true
      return null
    end

    irc_msg "New Commits for \'" + json['repository']['name'] + "\'"

    json['commits'].each do |commit|
      irc_msg "by #{commit['author']['name']} | #{commit['message']} | #{commit['url']}"
    end

  end

  def merge_request(json)

    case json['object_attributes']['action']
    when 'open'
      irc_msg json['user']['name'] + " opened merge request \'" + json['object_attributes']['title'] + "\' | " + json['object_attributes']['url']
    when 'update'
      irc_msg "Merge request '#{json['object_attributes']['title']}'#{json['object_attributes']['project_id']} updated."
    when 'merge'
      irc_msg "Merge request '#{json['object_attributes']['title']}'#{json['object_attributes']['project_id']} merged by #{json['user']['name']}."
    when 'close'
      irc_msg "Merge request '#{json['object_attributes']['title']}'#{json['object_attributes']['project_id']} closed by #{json['user']['name']}."
    else
      puts 'Invalid merge request action: ' + json['object_attributes']['action']
    end

  end


  def issue(json)
    puts "**issue** and json: \n" + json
    irc_msg "hello world"
    case json['object_attributes']['action']
    when 'open'
      irc_msg "New issue by " + json['user']['name'] + ": " + json['object_attributes']['title'] + " " + json['object_attributes']['url']
    when 'update'
      puts "Oh look someone updated an issue!"
    when 'reopen'
      irc_msg "Issue #" + json['object_attributes']['iid'] + " re-opened by" + json['user']['name']
    when 'close'
      irc_msg "Issue #" + json['object_attributes']['iid'] + " closed by " + json['user']['name']
    end
  end

  def api_request(url, params)
    url = URI.encode(url.to_s)
    uri = URI.parse(url)

    if !(defined? params)
      params = {}
    end

    params[:private_token] = @gitlab['private-key'].to_s

    uri.query = URI.encode_www_form(params)
    puts uri

    res = Net::HTTP.get_response(uri)
    return res.body
  end

  def request_issue(reqid)
    reqid = reqid[0]
    response = api_request "#{@apiurl}projects/#{@gitlab['project-id']}/issues", {:iid => reqid.to_s}
    response = JSON.parse(response)[0]
    puts response
    irc_msg "Gitlab issue ##{reqid}: '#{response['title']}' - #{@projecturl}/issues/#{response['id']}"
  end

  def request_mr(reqid)
    reqid = reqid[0]
    response = api_request "#{@apiurl}projects/#{@gitlab['project-id']}/merge_requests", {:iid => reqid.to_s}
    response = JSON.parse(response)[0]
    puts response
    irc_msg "Gitlab MR ##{reqid}: '#{response['title']}' - #{@projecturl}/merge_requests/#{response['iid'}"
  end

  def request_sha(sh)
    sha = sh[0][0]
    response = api_request "#{@apiurl}projects/#{@gitlab['project-id']}/repository/commits/#{sha[0]}", {}
    response = JSON.parse(response)
    irc_msg "Commit \"#{response['title']}\" | #{response['author_name']} - #{@projecturl}/commit/#{sha[0]}"
  end
end



bot = Bot.new()

# Gitlab CI build notifications
post '/ci' do
  Thread.new do
    json = JSON.parse(request.env["rack.input"].read)
    p json

    case json['build_status']
    when 'failed'
      bot.irc_msg "Build failed for build id #{json['build_id']} | #{json['push_data']['commits'][0]['url']}"
    when 'success'
      bot.irc_msg "Build successful for build id #{json['build_id']} | #{json['push_data']['commits'][0]['url']}"
    else
      logger.debug "Build status given of type #{json['build_status']}."
    end
  end
end

post '/commit' do
  Thread.new do
    json = JSON.parse(request.env["rack.input"].read)
    case json['object_kind']
    when 'push'
      puts "*commit*"
      bot.commits json
    when 'issue'
      puts "*issue*"
      bot.issue json
    when 'merge_request'
      puts "*merge request*"
      bot.merge_request json
    else
      puts "Invalid or unhandled request: " + json['object_kind']
    end
  Thread.stop
  end
end
